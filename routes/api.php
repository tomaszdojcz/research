<?php declare(strict_types=1);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MonitorController;

Route::prefix('monitors')->controller(MonitorController::class)->group(function() {
    Route::get('/{url}', 'show')->where('url', '.*');
    Route::post('/', 'store');
});
