<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Url extends Model
{
    use HasFactory;

    protected $fillable = ['url'];

    protected $with = ['stats'];

    protected $url;

    public function getId(): int
    {
        return (int) $this->attributes['id'];
    }

    public function getUrl(): string
    {
        return $this->attributes['url'];
    }

    public function setUrl($url): void
    {
        $this->attributes['url'] = $url;
    }

    public function stats(): HasOne
    {
        return $this->hasOne(Stat::class);
    }
}
