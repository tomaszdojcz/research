<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    use HasFactory;

    protected $fillable = ['url_id', 'redirect_time', 'redirect_amount'];

    public function getUrlId(): int
    {
        return $this->attributes['url_id'];
    }

    public function setUrlId(int $urlId): void
    {
        $this->attributes['url_id'] = $urlId;
    }

    public function getRedirectionTime(): int
    {
        return $this->attributes['redirect_time'];
    }

    public function setRedirectionTime(int $redirectionTime): void
    {
        $this->attributes['redirect_time'] = $redirectionTime;
    }

    public function getRedirectionsAmount(): int
    {
        return $this->attributes['redirect_amount'];
    }

    public function setRedirectionsAmount(int $redirectionsAmount): void
    {
        $this->attributes['redirect_amount'] = $redirectionsAmount;
    }
}
