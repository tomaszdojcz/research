<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Url;

class UrlUpdater
{
    public function update(array $urls): void
    {
        foreach ($urls as $url) {
            $url = str_contains($url, "https://") || str_contains($url, "http://") ? $url : "https://" . $url;
            (Url::updateOrCreate(['url' => rtrim($url, '/')]))->save();
        }
    }
}
