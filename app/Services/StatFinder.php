<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Stat;
use App\Models\Url;

class StatFinder
{
    public function find(string $url): ?Stat
    {
        $url = str_contains($url, "https://") || str_contains($url, "http://")? $url : "https://" . $url;
        $url = Url::where('url', $url)
            ->first();

        return $url ? $url->stats()->first() : null;
    }
}
