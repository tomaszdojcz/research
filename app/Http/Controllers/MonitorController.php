<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\StatFinder;
use App\Services\UrlFinder;
use App\Services\UrlUpdater;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MonitorController extends Controller
{
    public function __construct(private UrlUpdater $updater, private StatFinder $statFinder)
    {
    }

    public function show(string $url): JsonResponse
    {
        $stats = $this->statFinder->find($url);

        return response()
            ->json([
                'stats' => [
                    'redirect_time' => $stats->getRedirectionTime(),
                    'redirect_amount' => $stats->getRedirectionsAmount()
                ]
            ]);
    }

    public function store(Request $request): JsonResponse
    {
        $urls = $request->get('urls', []);
        $this->updater->update($urls);

        return response()
            ->json([
                'urls' => $urls
            ]);
    }
}
