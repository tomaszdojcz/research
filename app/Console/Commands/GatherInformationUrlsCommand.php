<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Stat;
use App\Models\Url;
use Illuminate\Bus\Queueable;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class GatherInformationUrlsCommand extends Command implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $signature = 'gather:information';

    protected $description = 'Gather information about redirects visiting urls';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $urlEntities = Url::all();

        foreach ($urlEntities as $urlEntity) {
            $response = Http::get($urlEntity->getUrl());
            /** @var Stat $stats */
            $stats = $urlEntity->stats()->first();

            if ($response->redirect() && null !== $stats) {
                $stats->setUrlId($urlEntity->getId());
                $stats->setRedirectionsAmount($stats->getRedirectionsAmount() + 1);
                $stats->setRedirectionTime($stats->getRedirectionTime() + $response->handlerStats()['redirect_time']);
                $stats->save();
            }
        }
    }
}
