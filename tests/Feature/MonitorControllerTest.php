<?php declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

class MonitorControllerTest extends TestCase
{
    public function test_show(): void
    {
        $response = $this->get('http://127.0.0.1:8000/api/monitors/onet.pl');

        $response->assertStatus(200);
    }

    public function test_store(): void
    {
        $response = $this->post('http://127.0.0.1:8000/api/monitors');

        $response->assertStatus(200);
    }
}
